<?php
$view = views_new_view();
$view->name = 'dacast_videos';
$view->description = 'Default view created by Dacast View module.';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Dacast Videos';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Dacast Videos';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '9';
$handler->display->display_options['style_plugin'] = 'grid';
$handler->display->display_options['style_options']['columns'] = '3';
$handler->display->display_options['style_options']['fill_single_line'] = 1;
$handler->display->display_options['row_plugin'] = 'fields';
/* Field: Content: Title */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
$handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['title']['alter']['trim'] = 0;
$handler->display->display_options['fields']['title']['alter']['html'] = 0;
$handler->display->display_options['fields']['title']['hide_empty'] = 0;
$handler->display->display_options['fields']['title']['empty_zero'] = 0;
$handler->display->display_options['fields']['title']['link_to_node'] = 1;
/* Field: Content: Description. */
$handler->display->display_options['fields']['dacast_video_media_description']['id'] = 'dacast_video_media_description';
$handler->display->display_options['fields']['dacast_video_media_description']['table'] = 'field_data_dacast_video_media_description';
$handler->display->display_options['fields']['dacast_video_media_description']['field'] = 'dacast_video_media_description';
$handler->display->display_options['fields']['dacast_video_media_description']['label'] = '';
$handler->display->display_options['fields']['dacast_video_media_description']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['dacast_video_media_description']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['dacast_video_media_description']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['dacast_video_media_description']['alter']['external'] = 0;
$handler->display->display_options['fields']['dacast_video_media_description']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['dacast_video_media_description']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['dacast_video_media_description']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['dacast_video_media_description']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['dacast_video_media_description']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['dacast_video_media_description']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['dacast_video_media_description']['alter']['trim'] = 0;
$handler->display->display_options['fields']['dacast_video_media_description']['alter']['html'] = 0;
$handler->display->display_options['fields']['dacast_video_media_description']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['dacast_video_media_description']['element_default_classes'] = 1;
$handler->display->display_options['fields']['dacast_video_media_description']['hide_empty'] = 0;
$handler->display->display_options['fields']['dacast_video_media_description']['empty_zero'] = 0;
$handler->display->display_options['fields']['dacast_video_media_description']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['dacast_video_media_description']['type'] = 'text_trimmed';
$handler->display->display_options['fields']['dacast_video_media_description']['settings'] = array(
  'trim_length' => '80',
);
$handler->display->display_options['fields']['dacast_video_media_description']['field_api_classes'] = 0;
/* Field: Content: Media thumbnail */
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['id'] = 'dacast_video_media_thumbnail';
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['table'] = 'field_data_dacast_video_media_thumbnail';
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['field'] = 'dacast_video_media_thumbnail';
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['label'] = '';
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['alter']['alter_text'] = 1;
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['alter']['text'] = '<img src=\'[dacast_video_media_thumbnail]\' />';
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['alter']['external'] = 0;
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['alter']['trim'] = 0;
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['alter']['html'] = 0;
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['element_default_classes'] = 1;
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['hide_empty'] = 0;
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['empty_zero'] = 0;
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['dacast_video_media_thumbnail']['field_api_classes'] = 0;
/* Sort criterion: Content: Post date */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'node';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['order'] = 'DESC';
/* Filter criterion: Content: Published */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 0;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Content: Type */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'dacast_video' => 'dacast_video',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'dacast-videos';
